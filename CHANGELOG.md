# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## 1.0.0 (2020-03-22)


### Features

* init commit - panic relief ([30f1608](https://github.com/icelam/panic-relief/commit/30f1608094b917e0f58302daa88b6b828d5a7a81))
